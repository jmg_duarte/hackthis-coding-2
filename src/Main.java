import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner intParser = new Scanner(System.in);
		String stringToSplit = intParser.nextLine();
		intParser.close();
		String[] numbersToParse = stringToSplit.split(",");
		int[] parsedNumbers = new int[numbersToParse.length];
		char[] numbersToString = new char[numbersToParse.length];
		int i = 0;
		for(String number : numbersToParse){
			if (!number.equals(" ")) {
				parsedNumbers[i] = Integer.parseInt(number);
				parsedNumbers[i] = parsedNumbers[i] + 32;
				numbersToString[i] = (char) parsedNumbers[i];
			} else {
				parsedNumbers[i] = 32;
				numbersToString[i] = (char) parsedNumbers[i];
			}
			i++;
		}
		for(char in : numbersToString){
			System.out.print(in);
		}
	}

}
